# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Changes from revision (TBD)


## [0.5.5] - 2022-02-16

### Added
- Lägger till exempel på datamängd i CSV-, ODS- och XLSX-format.
- Lägg till terminologi
- Publicerat som specifikation på [https://www.dataportal.se/sv/specifications/lekplatser/1.0](https://www.dataportal.se/sv/specifications/lekplatser/1.0).
- Publicerat webbsida på [https://lankadedata.se/spec/lekplatser/](https://lankadedata.se/spec/lekplatser/).

### Fixed
- Typos

## [0.5.0] - 2022-02-16
### Added
- Lagt till JSON-schema.
- Lagt upp på [https://gitlab.com/lankadedata/spec/lekplatser](https://gitlab.com/lankadedata/spec/lekplatser).

### Changed
- modellbeskrivning

### Fixed
- 

[Unreleased]: 
