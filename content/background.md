
**Bakgrund**

Syftet till specifikationen är att presentera information om lekplatser på att enkelt och likvärdigt sätt.

Specifikationen syftar till att ge deltagare i Dataportal Väst (och Sverige och internationellt) möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver lekplatser på ett enhetligt sätt.

Vi har också haft stöd från följande organisationer:

**[Västra Götalandsregionen](https://www.vgregion.se/)** - Behov och bidrag.<br>
**[MetaSolutions](https://entryscape.com/en/)** - Tips, stöd och råd, samt teknisk bas för specifikantionshantering.<br>