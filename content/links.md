# Länkar


## Open Street Map

Under arbetet med denna specifikation valde vi att utgå från Open Street Maps mappning gällande lekplatser och dess anordningar. Vi önskade att specifikationen för lekplatser blir kompatibel med och kan användas av gemenskapen på bästa sätt. 

## Tillgänglighetsdatabasen

Tillgänglighetsdatabasen (TD) saknar persistenta identifierare som är stabila över tid och det går inte att hänvisa till ett objekt på ett stabilt sätt annat än med hjälp av URL direkt till objektet som beskrivs. Tips har lämnats till TD om att en sådan identifierare kanske kunde skapas och göras synlig/tydlig i API och på webbplatsen, så att länkning av data kan ske på ett bra sätt. 

TD har ett API som du kan nå på
[Tillgänglighetsdatabasen - developer portal (azure-api.net)](https://td.portal.azure-api.net/) och som är kostnadsfritt. För att använda API:erna måste du dock registrera dig och hämta ut en personlig nyckel. APIt och dess utveckling sköts helt av Tillgänglighetsdatabasen och frågor om dess kapabilitet och funktion hänvisar vi till TD. Lämna gärna förbättringsförslag till TD som har indikerat till oss att de är intresserade av att deras tjänster nyttjas och utvecklas.

### Min lekplats saknas i Tillgänglighetsdatabasen

Kontakta din kommun där lekplatsen ligger och be dem lägga till lekplatsen. Generellt saknas en del lekplatser.