# Introduktion
Med lekplatser menas en särskilt anlagd allmän plats där lek och umgänge kan ske. I texten nedan används “lekplats” för att benämna detta.

Denna specifikation definierar en enkel tabulär informationsmodell för lekplatser. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten CSV och JSON. Som grund förutsätts CSV kunna levereras då det är ett praktiskt format och skapar förutsägbarhet för mottagare av informationen. Det är också ett format som gör sammanställningen av datamängden enklare då den kan representeras i t.ex. ett kalkylblad.
